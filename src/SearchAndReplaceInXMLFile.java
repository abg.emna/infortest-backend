import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class SearchAndReplaceInXMLFile implements ISearchAndReplace{

    @Override
    public void searchAndReplace(String oldString, String newString, Path inFile, Path outfile) {

        try (Stream<String> lines = Files.lines(inFile)) {
            AtomicBoolean commentFlag = new AtomicBoolean(false);
            List<String> replaced = lines
                    .map(line -> {
                        if (!line.isEmpty()) {
                            if (line.trim().startsWith("<!--") && line.trim().endsWith("-->")) {
                                commentFlag.set(true);
                            } else if (line.trim().startsWith("<!--") || line.trim().startsWith("<comment")) {
                                commentFlag.set(true);
                            } else if (line.trim().endsWith("-->") || line.trim().endsWith("</comment>")) {
                                commentFlag.set(false);
                            } else if (!commentFlag.get() && line.matches(".*=.*"+oldString+".*")) {
                                line = line.replaceAll(oldString, newString);
                            }
                        }
                        return line;
                    })
                    .collect(Collectors.toList());
            Files.write(outfile, replaced, StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

import java.nio.file.Path;
import java.nio.file.Paths;

public class main {

    public static void main(String[] args) throws Exception {
        ISearchAndReplace searchAndReplace;

        String fileType = args[0];
        String oldString = args[1];
        String newString = args[2];
        Path inFile = Paths.get("./src/testFiles/"+args[3]);
        Path outFile = Paths.get("./src/testFiles/"+args[4]);
        switch (fileType) {
            case "xml":
                searchAndReplace = new SearchAndReplaceInXMLFile();
                searchAndReplace.searchAndReplace(oldString, newString, inFile, outFile);
                break;
            case "txt":
                searchAndReplace = new SearchAndReplaceInTXTFile();
                searchAndReplace.searchAndReplace(oldString, newString, inFile, outFile);
                break;
            default:
                throw new Exception("This file type is not considered");
        }
    }
}

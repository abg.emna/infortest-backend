import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SearchAndReplaceInTXTFile implements ISearchAndReplace{
    @Override
    public void searchAndReplace(String oldString, String newString, Path inFile, Path outfile) {

        try (Stream<String> lines = Files.lines(inFile)) {
            List<String> replaced = lines
                    .map(line -> {
                        if (!line.isEmpty()) {
                            line.replaceAll(oldString, newString);
                        }
                        return line;
                    })
                    .collect(Collectors.toList());
            Files.write(outfile, replaced, StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

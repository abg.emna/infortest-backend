import java.nio.file.Path;

public interface ISearchAndReplace {
    void searchAndReplace(String oldString, String newString, Path inFile, Path outfile);
}
